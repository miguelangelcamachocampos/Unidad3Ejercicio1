<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="d" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<table border="1">
		<tr>
			<th>
			  <form action="TransportistaController">
			  		<input type="submit" name="btn_new"
			  		value="New "/>
			  </form>
			  <a href="TransportistaReport">Print report</a>
			</th>
			<td>Id</td>
			<td>License number:</td>
			<td>Category</td>
			<td>Placa</td>
		</tr>
		 <d:forEach var="transportista" items="${transportistas}">
		 	 <tr>
		 	 	<td>
		 	 		<form action="TransportistaController">
		 	 			<input type="hidden" name="id" value="${transportista.id }">
		 	 			<input type="submit" name="btn_edit" value="Edit"/>
		 	 			 <input type="submit" name="btn_delete" value="Delete"/>
		 	 		</form>
		 	 	</td>
		 	 	<td>${transportista.id }</td>
		 	 	<td>${transportista.numeroLicencia }</td>
		 	 	<td>${transportista.categoria }</td>
		 	 	<td>${transportista.placa }</td>
		 	 </tr>	
		 </d:forEach>
	</table>
</body>
</html>