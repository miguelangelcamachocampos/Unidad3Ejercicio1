package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Transportista;
import dao.TransportistaDAO;
import dao.TransportistaDAOImpl;

/**
 * Servlet implementation class TransportistaController
 */
@WebServlet("/TransportistaController")
public class TransportistaController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Transportista transportista;
	private List<Transportista> transportistas;
	private List<String> ids = new ArrayList<String>();
	private TransportistaDAO transportistaDAO;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TransportistaController() {
		super();
		transportista = new Transportista();
		transportistas = new ArrayList<Transportista>();
		transportistaDAO = new TransportistaDAOImpl();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getParameter("btn_save") != null) {
			transportista.setNumeroLicencia(request.getParameter("numeroLicencia"));
			transportista.setCategoria(request.getParameter("categoria"));
			transportista.setPlaca(request.getParameter("placa"));
			System.out.println("save");
			if (transportista.getId() == 0) {
				System.out.println("save");
				transportistaDAO.createTransportista(transportista);
			} else {
				System.out.println("save");
				transportistaDAO.updateTransportista(transportista);
			}
			transportistas = transportistaDAO.readAllTransportistas();
			request.setAttribute("transportistas", transportistas);
			request.getRequestDispatcher("transportista_list.jsp").forward(request, response);
		} else if (request.getParameter("btn_new") != null) {
			transportista = new Transportista();
			request.getRequestDispatcher("transportista_form.jsp").forward(request, response);
		} else if (request.getParameter("btn_edit") != null) {
			try {
				int id = Integer.parseInt(request.getParameter("id"));
				transportista = transportistaDAO.readTransportista(id);
			} catch (IndexOutOfBoundsException e) {
				transportista = new Transportista();
			}
			request.setAttribute("transportista", transportista);
			request.getRequestDispatcher("transportista_form.jsp").forward(request, response);
		} else if (request.getParameter("btn_delete") != null) {
			try {
				int id = Integer.parseInt(request.getParameter("id"));
				transportistaDAO.deleteTransportista(id);
				transportistas = transportistaDAO.readAllTransportistas();
			} catch (Exception e) {
				e.printStackTrace();
			}
			request.setAttribute("transportistas", transportistas);
			request.getRequestDispatcher("transportista_list.jsp").forward(request, response);
		} else {
			transportistas = transportistaDAO.readAllTransportistas();
			request.setAttribute("transportistas", transportistas);
			request.getRequestDispatcher("transportistas_list.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
