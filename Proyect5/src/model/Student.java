/**
 * 
 */
package model;

import java.io.Serializable;

/**
 * @author usuario
 *
 */
public class Student implements Serializable{
	private Long id;
	private String name;
	private String groupStudent;
	private String turn;
	
	public Student (Long id, String name, String groupStudent, String turn) {
		super();
		this.id = id;
		this.name = name;
		this.groupStudent = groupStudent;
		this.turn = turn;
	}
	
	public Student() {
		this(0L, "", "","");
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the groupStudent
	 */
	public String getGroupStudent() {
		return groupStudent;
	}

	/**
	 * @param groupStudent the groupStudent to set
	 */
	public void setGroupStudent(String groupStudent) {
		this.groupStudent = groupStudent;
	}

	/**
	 * @return the turn
	 */
	public String getTurn() {
		return turn;
	}

	/**
	 * @param turn the turn to set
	 */
	public void setTurn(String turn) {
		this.turn = turn;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", groupStudent=" + groupStudent + ", turn=" + turn + "]";
	}
	
	
	
}
