package model;

import java.io.Serializable;

public class Transportista implements Serializable{
	private int id;
	private String numeroLicencia;
	private String categoria;
	private String placa;
	
	
	public Transportista(int id,String numeroLicencia,String categoria,String placa) {
		super();
		this.id = id;
		this.numeroLicencia = numeroLicencia;
		this.categoria = categoria;
		this.placa = placa;
	}
	
	public Transportista() {
		this(0, "", "", "");
	}


	/**
	 * @return the numeroLicencia
	 */
	public String getNumeroLicencia() {
		return numeroLicencia;
	}

	/**
	 * @param numeroLicencia the numeroLicencia to set
	 */
	public void setNumeroLicencia(String numeroLicencia) {
		this.numeroLicencia = numeroLicencia;
	}

	/**
	 * @return the categoria
	 */
	public String getCategoria() {
		return categoria;
	}

	/**
	 * @param categoria the categoria to set
	 */
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	/**
	 * @return the placa
	 */
	public String getPlaca() {
		return placa;
	}

	/**
	 * @param placa the placa to set
	 */
	public void setPlaca(String placa) {
		this.placa = placa;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Transportista [id=" + id + ", numeroLicencia=" + numeroLicencia + ", categoria=" + categoria
				+ ", placa=" + placa + "]";
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	
	
	
}
