/**
 * 
 */
package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Transportista;
/**
 * @author Miguel
 *
 */
public class TransportistaDAOImpl implements TransportistaDAO {
	private Connection connection;
	private PreparedStatement prepareStatement;
	private ResultSet resultSet;

	public  TransportistaDAOImpl() {
		getConnection();
	}

	public Connection getConnection() {
		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/ejercicio1", "utng", "mexico");
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return connection;
	}

	@Override
	public void createTransportista(Transportista transportista) {
		if (connection != null) {
			try {
				prepareStatement = 
						connection.prepareStatement(
						"INSERT INTO transportista (numero_licencia,categoria,placa) values(?,?,?);");
				prepareStatement.setString(1, transportista.getNumeroLicencia());
				prepareStatement.setString(2, transportista.getCategoria());
				prepareStatement.setString(3, transportista.getPlaca());
				prepareStatement.execute();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public Transportista readTransportista(int id) {
		Transportista transportista = null;
		if (connection != null) {
			try {
				prepareStatement = connection.prepareStatement("SELECT * FROM transportista WHERE id=?;");
				prepareStatement.setInt(1, id);
				resultSet = prepareStatement.executeQuery();
				if (resultSet.next()) {
					transportista = new Transportista(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3),
							resultSet.getString(4));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return transportista;
	}

	@Override
	public List<Transportista> readAllTransportistas() {
		List<Transportista> transportistas = new ArrayList<Transportista>();
		if (connection != null) {
			try {
				prepareStatement = connection.prepareStatement("SELECT * FROM transportista;");
				resultSet = prepareStatement.executeQuery();
				while (resultSet.next()) {
					Transportista transportista = new Transportista(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3),
							resultSet.getString(4));
					transportistas.add(transportista);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return transportistas;
	}

	@Override
	public void updateTransportista(Transportista transportista) {
		if (connection != null) {
			try {
				prepareStatement = connection
						.prepareStatement("UPDATE transportista  SET numero_licencia = ?, categoria=?," + " placa=?  WHERE id=?;");
				prepareStatement.setString(1, transportista.getNumeroLicencia());
				prepareStatement.setString(2, transportista.getCategoria());
				prepareStatement.setString(3, transportista.getPlaca());
				prepareStatement.setInt(4, transportista.getId());
				prepareStatement.execute();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void deleteTransportista(int id) {
		if (connection != null) {
			try {
				prepareStatement = connection.prepareStatement("DELETE FROM transportista  WHERE id=?;");
				prepareStatement.setInt(1, id);
				prepareStatement.execute();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
