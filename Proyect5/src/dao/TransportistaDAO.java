package dao;

import java.util.List;

import model.Student;
import model.Transportista;

public interface TransportistaDAO {
	void createTransportista(Transportista transportista);
	Transportista readTransportista(int id);
	List<Transportista> readAllTransportistas();
	void updateTransportista(Transportista transportista);
	void deleteTransportista(int id);
}
